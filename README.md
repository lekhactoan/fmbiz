# fmbiz

### Compiles and hot-reloads for development
```
docker-compose up
```

### Run your tests web
```
docker exec -it fmbiz_web bash
npm run test
```

### Lints and fixes files web
```
docker exec -it fmbiz_web bash
npm run lint
```

### Run your tests api
```
docker exec -it fmbiz_api bash
npm run test
```

### Lints and fixes files api
```
docker exec -it fmbiz_api bash
black .
flake8 .
```