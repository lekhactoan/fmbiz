export const COLORS = [
  {
    name: 'white',
    code: '#f9f9eb'
  },
  {
    name: 'black',
    code: '#000000'
  },
  {
    name: 'gray',
    code: '#808080'
  },
  {
    name: 'purple',
    code: '#800080'
  },
  {
    name: 'brown',
    code: '#663300'
  },
  {
    name: 'red',
    code: '#ff0000'
  },
  {
    name: 'pink',
    code: '#ff3399'
  },
  {
    name: 'orange',
    code: '#ff7733'
  },
  {
    name: 'yellow',
    code: '#ffff00'
  },
  {
    name: 'Beige',
    code: '#e9e9af'
  },
  {
    name: 'blue',
    code: '#0000ff'
  },
  {
    name: 'green',
    code: '#00ff00'
  }
]
