export const UNITS = [
  {
    id: 1,
    name: '本'
  },
  {
    id: 2,
    name: '枚'
  },
  {
    id: 3,
    name: '束'
  },
  {
    id: 4,
    name: '袋'
  },
  {
    id: 5,
    name: '個'
  },
  {
    id: 6,
    name: 'kg'
  },
  {
    id: 7,
    name: '箱'
  },
  {
    id: 8,
    name: 'リン'
  },
  {
    id: 1,
    name: 'unit 1'
  },
  {
    id: 1,
    name: 'unit 2'
  },
  {
    id: 1,
    name: 'unit 3'
  },
  {
    id: 1,
    name: 'unit 4'
  }
]
