import Color from '../Color'

export default interface Order {
  item: string
  variety: string
  colors: Array<Color>
  quality: string
  size: string
  quantity: number
  unit: string
  boxes: number
}
