export default interface Color {
  name: string
  code: string
}
