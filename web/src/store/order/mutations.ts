import { Order, Color } from 'typings'

export const mutations = {
  updateSelectedColors(state: { detail: Order }, color: Color): void {
    const index = state.detail.colors.findIndex((item) => item.code === color.code)
    if (index !== -1) {
      state.detail.colors.splice(index, 1)
    } else {
      state.detail.colors.push(color)
    }
  }
}
