export const state = {
  detail: {
    item: 'アーティチョーク',
    variety: 'サランボー',
    colors: [
      {
        name: 'red',
        code: '#ff0000'
      },
      {
        name: 'pink',
        code: '#ff3399'
      },
      {
        name: 'orange',
        code: '#ff7733'
      },
      {
        name: 'yellow',
        code: '#ffff00'
      }
    ],
    quality: '等級',
    size: '2L 以上',
    quantity: 50,
    unit: '本',
    boxes: 2
  }
}
